/**
 * This is a template files. If you want to execute this firmware 
 * on your device, uncomment the declaration of the variable 
 * privateKey and assign your private key to that variable.
 * 
 * After that, remove the string '-example' from the file name.
 * Keep in mind: Never commit your secrets to an repository!
 */
// const char* privateKey = "<your uint16 encoded private key>";
