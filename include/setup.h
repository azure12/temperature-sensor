#ifndef SETUP_H
#define SETUP_H

#define NTP_PRIMARY_DOMAIN "pool.ntp.org"
#define NTP_SECONDARY_DOMAIN "time.nist.gov"

#include "secrets.h"

extern CloudIoTCoreDevice *device;
extern Client *netClient;
extern MQTTClient *mqttClient;
extern CloudIoTCoreMqtt *mqtt;
extern Adafruit_BME280 bme;
extern unsigned long delayTime;
extern const char* privateKey;
extern unsigned long iat;

void setupGpio();
void setupWifi();
void startupSensor();
void setupCloudIoT();
String getJwt();

#endif /* SETUP_H */
