/**
 * This is a template files. If you want to execute this firmware 
 * on your device, uncomment the declaration of the variable 
 * privateKey and assign your private key to that variable.
 * 
 * After that, remove the string '-example' from the file name.
 * Keep in mind: Never commit your secrets to an repository!
 */

#define WIFI_SSID "<WiFi SSID (name)>"
#define WIFI_PASSWORD "<WiFi password>"

// Cloud iot details.
#define PROJECT_ID "<project id>"
#define LOCATION "<region>"
#define REGISTRY_ID  "<registry id>"
#define DEVICE_ID  "device id"

const char* privateKey = "<binary representation of private key>"