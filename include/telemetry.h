#ifndef TELEMETRY_H
#define TELEMETRY_H

#include "main.h"

extern CloudIoTCoreMqtt *mqtt;
extern MQTTClient *mqttClient;
extern CloudIoTCoreDevice *device;

bool publishTelemetry(String subtopic, float value);

bool publishTelemetry(String data);

bool publishTelemetry(float value);

bool publishState(String data);

#endif /* TELEMETRY_H */