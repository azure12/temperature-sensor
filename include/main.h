#ifndef MAIN_H
#define MAIN_H

#define SENSOR_IDENTIFIER "living-room-desk"
#define PUBLISH_INTERVAL_SEC 60
#define JWT_VALIDITY_MINUTES 20
#define JWT_REFRESH_THRESHOLD_SECONDS 600

#define RESET 34
#define LED_BLUE 12
#define LED_GREEN 14
#define LED_YELLOW 26
#define LED_RED 32

#include <MQTT.h>

#include <CloudIoTCore.h>
#include <CloudIoTCoreMqtt.h>

#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>

void messageReceived(String &topic, String &payload);

#endif /* MAIN_H */